const axios = require('axios');
const fs = require('fs');
const regex_lastPage = '<a class="paginator-page.*<\/a>';
const regex_posts = '<article id="post_[0-9]*"'
const url = "https://danbooru.donmai.us/";
var total_images = 0;
var downloaded_images = 0;
var q_count = 0;
const myArgs = process.argv.slice(2);
const tag = myArgs[0];
const dir = "images/" + tag;
const download = myArgs[1] == 'true';

{
    let lookup = "posts?tags=";
    let final_url = url + lookup + tag;
    let buffer;
    let iterator;
    let nb_pages;

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }

    axios.get(final_url)
        .then(result => {
            iterator = result.data.matchAll(regex_lastPage);
            try{
                let matches = Array.from(iterator);
                buffer = matches[matches.length-1][0];
                buffer = buffer.match(">[0-9]*<")[0];
                nb_pages =  parseInt(buffer.substring(1,buffer.length-1));
                get_all_posts(nb_pages,url,tag);
            }
            catch(error){
                console.log("this tag doesn't exist");
                process.exit();
            }
        })
        .catch(error =>{console.log(error);});
}

function get_all_posts(nb_pages,url,tag){
    for(let i = 1; i<= nb_pages;i++){
        let lookup = "posts?page=" + i + "&tags=";
        let final_url = url + lookup + tag;
        get_page_posts(final_url,i,nb_pages);
    }
}
function get_page_posts(url, page,nb_pages, tries = 0){ 
    axios.get(url)
        .then(result => {
            let iterator = result.data.matchAll(regex_posts);
            posts_array = Array.from(iterator);
            for(let j = 0;j < posts_array.length;j++){
                buffer = posts_array[j][0];
                buffer = buffer.match('_[0-9]*"')[0];
                buffer = buffer.substring(1,buffer.length-1);
                get_image(buffer,page,nb_pages/7);
                //setTimeout(() => {get_image(buffer)},15000+page*2500);
                //console.log(buffer);
            }
            console.log("page " + page + " recovered succesfully");
        })
        .catch(error =>{
            console.log("request failed on page " + page + ", retrying in a few seconds ...");
            setTimeout(() => {get_page_posts(url,page,tries++);},5000 *1.25**tries);
            //console.log(error);
        })
}

function get_image(post_id,page,wait_coeff,tries = 0){
    setTimeout(() =>{
        let final_url = url + "posts/" + post_id + ".json";
        axios.get(final_url)
            .then(result => {
                total_images++;
                if(result.data.rating != "e" && result.data.file_ext != "mp4" && result.data.file_ext != "zip"){
                    if(result.data.rating == "q")
                        q_count++;
                    let name = tag + "_" + post_id + "." + result.data.file_ext;
                    if(download == true)
                        download_image(result.data.file_url,name);
                    downloaded_images++;
                    console.log("image " + name + " from page " + page + " treated" + " rating = " + result.data.rating + " total images downloaded/seen/questionnable = " + downloaded_images + "/" + total_images + "/" + q_count);
                }
                else
                    console.log("post " + post_id + " ignored");
            })
            .catch(error =>{
                console.log(error);
                console.log("failed to fetch image, retrying soon ...");
                setTimeout(() => {get_image(final_url,page,tries++);},1000*1.25**tries);
            })
    },wait_coeff*1000+page*3000)
}

function download_image (url, file_name){
    axios({
        url,
        responseType: 'stream',
      }).then(
        response =>
          new Promise((resolve) => {
            response.data
              .pipe(fs.createWriteStream("images/" + tag + "/" + file_name))
              .on('finish', () => resolve())
              .on('error', () => {setTimeout(() => {download_image(img_url,file_name);},1000);});
          }),
      );
}

